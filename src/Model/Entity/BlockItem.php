<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BlockItem Entity
 *
 * @property int $id
 * @property int $block_id
 * @property string $text
 * @property string $link
 *
 * @property \App\Model\Entity\Block $block
 */
class BlockItem extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'block_id' => true,
        'text' => true,
        'link' => true,
        'block' => true
    ];
}
