<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;

$this->layout = false;

if (!Configure::read('debug')) :
    throw new NotFoundException(
        'Please replace src/Template/Pages/home.ctp with your own version or re-enable debug mode.'
    );
endif;

$cakeDescription = 'CakePHP: the rapid development PHP framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    
    
    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->css('tooplate-style.css') ?>
   <?= $this->Html->css('style.css') ?>

    <!--linear icon css-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">
</head>
<body class="home">


 <header class="header_area">
            <div class="top_menu">
                <div class="container">
                    <div class="top_inner">
                        <div class="float-left">
                            <a href="#">Visit Us</a>
                            <a href="#">Online Support</a>
                        </div>
                        <div class="float-right">
                            <ul class="list header_socila">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-behance"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main_menu" id="mainNav">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <a class="navbar-brand logo_h" href="index.html"><img src="img/logo.png" alt=""><img src="img/logo-2.png" alt=""></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                            <ul class="nav navbar-nav menu_nav ml-auto">
                                <li class="nav-item active"><a class="nav-link" href="index.html">Home</a></li> 
                                <li class="nav-item"><a class="nav-link" href="about-us.html">About</a></li> 
                                <li class="nav-item submenu dropdown">
                                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item"><a class="nav-link" href="projects.html">Project</a></li>
                                        <li class="nav-item"><a class="nav-link" href="project-details.html">Project Details</a></li>
                                        <li class="nav-item"><a class="nav-link" href="elements.html">Elements</a></li>
                                    </ul>
                                </li> 
                                <li class="nav-item submenu dropdown">
                                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Blog</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item"><a class="nav-link" href="blog.html">Blog</a></li>
                                        <li class="nav-item"><a class="nav-link" href="single-blog.html">Blog Details</a></li>
                                    </ul>
                                </li> 
                                <li class="nav-item"><a class="nav-link" href="contact.html">Contact</a></li>
                            </ul>
                        </div> 
                    </div>
                </nav>
            </div>
        </header>


        <section id="project"  class="project">
            <div class="container">
                <div class="project-details">
                    
                    <div class="project-content">
                        <div class="gallery-content">
                            <div class="isotope">
                                <div class="row">
                                    <div class=" col-md-4 col-sm-12">
                                        <div class="item big-height">
                                            <img src="<?= $this->Url->build('files/Blocks/image/' . $blocks[0]->image); ?>" alt="portfolio image"/>
                                            <div class="isotope-overlay">
                                                <a href="project.html">
                                                    <span class="lnr lnr-link"></span>
                                                    
                                                </a>
                                                <h3>
                                                    <a href="project.html">
                                                        <?= $blocks[0]->headline ?>
                                                    </a>
                                                </h3>
                                                <p><?= $blocks[0]->text ?></p>
                                            </div><!-- /.isotope-overlay -->
                                        </div><!-- /.item -->
                                    </div><!-- /.col -->
                                    <div class="col-md-8 col-sm-12">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-xs-12">
                                                <div class="item">
                                                    <img src="<?= $this->Url->build('files/Blocks/image/' . $blocks[1]->image); ?>" alt="portfolio image"/>
                                                    <div class="isotope-overlay">
                                                        <a href="project.html">
                                                            <span class="lnr lnr-link"></span>
                                                            
                                                        </a>
                                                        <h3>
                                                            <a href="project.html">
                                                                <?= $blocks[1]->headline ?>
                                                            </a>
                                                        </h3>
                                                        <p><?= $blocks[1]->text ?></p>
                                                    </div><!-- /.isotope-overlay -->
                                                </div><!-- /.item -->
                                            </div><!-- /.col -->
                                            <div class="col-lg-6 col-md-6 col-xs-12">
                                                <div class="item">
                                                    <img src="<?= $this->Url->build('files/Blocks/image/' . $blocks[2]->image); ?>" alt="portfolio image"/>
                                                    <div class="isotope-overlay">
                                                        <a href="project.html">
                                                            <span class="lnr lnr-link"></span>
                                                            
                                                        </a>
                                                        <h3>
                                                            <a href="project.html">
                                                                <?= $blocks[2]->headline ?>
                                                            </a>
                                                        </h3>
                                                        <p><?= $blocks[2]->text ?></p>
                                                    </div><!-- /.isotope-overlay -->
                                                </div><!-- /.item -->
                                            </div><!-- /.col -->
                                        </div><!-- /.row-->
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-xs-12">
                                                <div class="item">
                                                    <img src="<?= $this->Url->build('files/Blocks/image/' . $blocks[3]->image); ?>" alt="portfolio image"/>
                                                    <div class="isotope-overlay">
                                                        <a href="project.html">
                                                            <span class="lnr lnr-link"></span>
                                                            
                                                        </a>
                                                        <h3>
                                                            <a href="project.html">
                                                                <?= $blocks[3]->headline ?>
                                                            </a>
                                                        </h3>
                                                        <p><?= $blocks[3]->text ?>
                                                        </p>
                                                    </div><!-- /.isotope-overlay -->
                                                </div><!-- /.item -->
                                            </div><!-- /.col -->
                                            <div class="col-lg-6 col-md-6 col-xs-12">
                                                <div class="item">
                                                    <img src="<?= $this->Url->build('files/Blocks/image/' . $blocks[4]->image); ?>" alt="portfolio image"/>
                                                    <div class="isotope-overlay">
                                                        <a href="project.html">
                                                            <span class="lnr lnr-link"></span>
                                                            
                                                        </a>
                                                        <h3>
                                                            <a href="project.html">
                                                                <?= $blocks[4]->headline ?>
                                                            </a>
                                                        </h3>
                                                        <p><?= $blocks[4]->text ?>
                                                        </p>
                                                    </div><!-- /.isotope-overlay -->
                                                </div><!--/.item -->
                                            </div><!-- /.col -->
                                        </div><!-- /.row-->

                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                                
                            </div><!--/.isotope-->
                        </div><!--/.gallery-content-->
                    </div><!--/.project-content-->

                </div><!--/.project-details-->


               
            </div><!--/.container-->

        </section><!--/.project-->



<section>
    <div class="container">
    <div class="cd-hero-slider">
                <div class="was-li selected">                    
                    <div class="cd-full-width">
                        <div class="container-fluid js-tm-page-content" data-page-no="1" data-page-type="gallery">
                            <div class="tm-img-gallery-container">
                                <div class="tm-img-gallery gallery-one">                                                 
                                  <?php foreach ($blocks as $k => $v): ?>                                  
                                    <div class="grid-item">
                                        <figure class="effect-bubba">
                                            <img src="<?= $this->Url->build('files/Blocks/image/' . $v->image); ?>" alt="Image" class="img-fluid tm-img">
                                            <figcaption>
                                                <h2 class="tm-figure-title"><?= $v->headline ?></h2>
                                                <p class="tm-figure-description"><?= $v->text ?></p>
                                                <a href="img/tm-img-01.jpg">View more</a>
                                            </figcaption>           
                                        </figure>
                                    </div>
                                  <?php endforeach ?>  
                                                                                                         
                                </div>                                 
                            </div>
                        </div>                                                    
                    </div>                    
                </div>             

            </div>

</div>
</section>



<?= $this->Html->script('jquery-3.2.1.min.js') ?>
<?= $this->Html->script('bootstrap.min.js') ?>

<script type="text/javascript">
    ;(function($){
    "use strict"
    
    
    var nav_offset_top = $('header').height() + 50; 
    /*-------------------------------------------------------------------------------
      Navbar 
    -------------------------------------------------------------------------------*/

    //* Navbar Fixed  
    function navbarFixed(){
        if ( $('.header_area').length ){ 
            $(window).scroll(function() {
                var scroll = $(window).scrollTop();   
                if (scroll >= nav_offset_top ) {
                    $(".header_area").addClass("navbar_fixed");
                } else {
                    $(".header_area").removeClass("navbar_fixed");
                }
            });
        };
    };
    navbarFixed();
    
    

})(jQuery)
</script>
</body>
</html>
