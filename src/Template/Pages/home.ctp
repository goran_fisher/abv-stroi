<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;

$this->layout = false;

if (!Configure::read('debug')) :
    throw new NotFoundException(
        'Please replace src/Template/Pages/home.ctp with your own version or re-enable debug mode.'
    );
endif;

$cakeDescription = 'CakePHP: the rapid development PHP framework';
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Construction - Free HTML Bootstrap Template</title>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
       
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
   
        <?= $this->Html->css('bootstrap.min.css') ?>
        <?= $this->Html->css('../custom-font/fonts.css') ?>

         
         <?= $this->Html->css('font-awesome.min.css') ?>

    <!-- <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet"> -->

        <?= $this->Html->css('bootsnav.css') ?>

        <?= $this->Html->css('jquery.fancybox.css') ?>
        <?= $this->Html->css('custom.css') ?>

        <?= $this->Html->css('tooplate-style.css') ?>
        <?= $this->Html->css('style.css') ?>
        <?= $this->Html->css('concat.min.css') ?>



    </head>
    <body>

        <!-- Preloader -->

        <div id="loading">
            <div id="loading-center">
                <div id="loading-center-absolute">
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                    <div class="object"></div>
                </div>
            </div>
        </div>

        <!--End off Preloader -->

        <!-- Header -->
        <header>
            <!-- Top Navbar -->
            <div class="top_nav">
                <div class="container">
                    <ul class="list-inline info">
                        <li><a href="#"><span class="fa fa-phone"></span> 1234 - 5678 - 9012</a></li>
                        <li><a href="#"><span class="fa fa-envelope"></span> support@Construct.com</a></li>
                        <li><a href="#"><span class="fa fa-clock-o"></span> Mon - Sat 9:00 - 19:00</a></li>
                    </ul>
                    <ul class="list-inline social_icon">
                        <li><a href=""><span class="fa fa-facebook"></span></a></li>
                        <li><a href=""><span class="fa fa-twitter"></span></a></li>
                        <li><a href=""><span class="fa fa-behance"></span></a></li>
                        <li><a href=""><span class="fa fa-dribbble"></span></a></li>
                        <li><a href=""><span class="fa fa-linkedin"></span></a></li>
                        <li><a href=""><span class="fa fa-youtube"></span></a></li>
                    </ul>           
                </div>
            </div><!-- Top Navbar end -->

            <!-- Navbar -->
            <nav class="navbar bootsnav">
              

                <div class="container">
                    <!-- Atribute Navigation -->
                  
                    <!-- Header Navigation -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand" href=""><img class="logo" src="img/logo.png" alt=""></a>
                    </div>
                    <!-- Navigation -->
                    <div class="collapse navbar-collapse" id="navbar-menu">
                        <ul class="nav navbar-nav menu">
                            <li><a href="">Home</a></li>                    
                            <li><a href="#about">About Us</a></li>
                            <li><a href="#services">Services</a></li>
                            <li><a href="#portfolio">Portfolio</a></li>
                            <li><a href="#contact_form">Contact Us</a></li>
                        </ul>
                    </div>
                </div>   
            </nav><!-- Navbar end -->
        </header><!-- Header end -->


                <section id="mainBanners">
            <div class="bgImage">
                <img src="./ДТЭК — Официальный сайт _ Крупнейший энергетический холдинг Украины_files/visokovoltnie-seti-1_s1_t162_i1370_orig_s1_t162_i2580.jpg"
                 class="distribution">
                <img src="./ДТЭК — Официальный сайт _ Крупнейший энергетический холдинг Украины_files/traditional-0_s1_t162_i171_s1_t162_i171.jpg"
                 class="traditional">
                <img src="./ДТЭК — Официальный сайт _ Крупнейший энергетический холдинг Украины_files/oil-0_s1_t162_i172_s1_t162_i172.jpg"
                 class="oil">
                <img src="./ДТЭК — Официальный сайт _ Крупнейший энергетический холдинг Украины_files/green-0_s1_t162_i173_s1_t162_i173.jpg"
                 class="green">
                <img src="./ДТЭК — Официальный сайт _ Крупнейший энергетический холдинг Украины_files/effective-0_s1_t162_i174_s1_t162_i174.jpg"
                 class="effective">
                <img src="./ДТЭК — Официальный сайт _ Крупнейший энергетический холдинг Украины_files/default-0_s1_t162_i175-2_s1_t162_i175.jpg"
                 class="default">
            </div>
            <div class="mainBanners-slogan">
                <div class="top-block">
                    <p class="distribution">
                        <span>Дистрибуция электроэнергии</span>
                    </p>
                    <p class="traditional">
                        <span>ДТЭК — Лидер
                            <br>ТОПЛИВНО - ЭНЕРГЕТИЧЕСКОГО РЫНКА УКРАИНЫ</span>
                    </p>
                    <p class="oil">
                        <span>ДТЭК — НЕФТЕГАЗ
                            <br>НЕФТЕГАЗОДОБыЧА И ПЕРЕРАБОТКА</span>
                    </p>
                    <p class="green">
                        <span>ДТЭК —
                            <br>ЗЕЛЕНАЯ ЭНЕРГЕТИКА И ИННОВАЦИИ</span>
                    </p>
                    <p class="effective">
                        <span>ДТЭК — это
                            <br>энергоэффективность на рынке Украины</span>
                    </p>
                    <h1 class="default">КРУПНЕЙШИЙ ЭНЕРГЕТИЧЕСКИЙ
                        <br> ХОЛДИНГ УКРАИНЫ — ДТЭК</h1>
                </div>
                <div class="bottom-block">
                    <div class="item distribution" data-section="distribution">
                        <!--noindex-->
                        <a href="https://grids.dtek.com/" rel="nofollow" target="_blank">Дистрибуция электроэнергии</a>
                        <!--/noindex-->
                    </div>
                </div>
            </div>
            <div class="mainBanners-items">
                <div class="item traditional" data-section="traditional">
                    <a href="https://dtek.com/#" class="item-clck"></a>
                    <div class="item-cntnt">
                        <p>
                            <!--noindex-->
                            <a class="link" href="https://energo.dtek.com/" rel="nofollow" target="_blank">
                                <span>Тепловая энергетика</span>
                            </a>
                            <!--/noindex-->
                        </p>
                        <ul>
                            <li>
                                <!--noindex-->
                                <a href="https://energo.dtek.com/business/coal_industry/" rel="nofollow" target="_blank">Добыча и обогащение угля</a>
                                <!--/noindex-->
                            </li>
                            <li>
                                <!--noindex-->
                                <a href="https://energo.dtek.com/business/generation/" rel="nofollow" target="_blank">Генерация электроэнергии</a>
                                <!--/noindex-->
                            </li>
                            <li>
                                <!--noindex-->
                                <a href="https://energo.dtek.com/business/distribution/" rel="nofollow" target="_blank">Дистрибуция электроэнергии</a>
                                <!--/noindex-->&nbsp;</li>
                            <li>
                                <!--noindex-->
                                <a href="https://energo.dtek.com/business/service/" rel="nofollow" target="_blank">Сервис</a>
                                <!--/noindex-->
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="item oil" data-section="oil">
                    <a href="https://dtek.com/#" class="item-clck"></a>
                    <div class="item-cntnt">
                        <p>
                            <!--noindex-->
                            <a class="link" href="https://oilandgas.dtek.com/" rel="nofollow" target="_blank">
                                <span>Нефть и газ</span>
                            </a>
                            <!--/noindex-->
                        </p>
                        <ul>
                            <li>
                                <!--noindex-->
                                <a href="https://oilandgas.dtek.com/ua/" rel="nofollow" target="_blank">ДТЭК Нефтегаз</a>
                                <!--/noindex-->
                            </li>
                            <li>
                                <!--noindex-->
                                <a href="https://oilandgas.dtek.com/about/assets/neftegazdobycha/" rel="nofollow" target="_blank">Нефтегаздобыча</a>
                                <!--/noindex-->
                            </li>
                            <li>
                                <!--noindex-->
                                <a href="https://oilandgas.dtek.com/about/assets/neftegazrazrabotka/" rel="nofollow" target="_blank">Нефтегазразработка</a>
                                <!--/noindex-->
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="item green" data-section="green">
                    <a href="https://dtek.com/#" class="item-clck"></a>
                    <div class="item-cntnt">
                        <p>
                            <!--noindex-->
                            <a class="link" href="https://renewables.dtek.com/" rel="nofollow" target="_blank">
                                <span>Зеленая энергетика</span>
                            </a>
                            <!--/noindex-->
                        </p>
                        <ul>
                            <li>
                                <!--noindex-->
                                <a href="https://renewables.dtek.com/business#botievckaya" rel="nofollow" target="_blank">Ботиевская ВЭС</a>
                                <!--/noindex-->
                            </li>
                            <li>
                                <!--noindex-->
                                <a href="https://renewables.dtek.com/business#trifanovskaya" rel="nofollow" target="_blank">Трифановская СЭС</a>
                                <!--/noindex-->
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="item effective" data-section="effective">
                    <a href="https://dtek.com/#" class="item-clck"></a>
                    <div class="item-cntnt">
                        <p>
                            <!--noindex-->
                            <a class="link" href="https://esco.dtek.com/" rel="nofollow" target="_blank">
                                <span data-mce-mark="1">Энергоэффективность</span>
                            </a>
                            <!--/noindex-->
                        </p>
                        <ul>
                            <li>
                                <!--noindex-->
                                <a style="font-size: 14px; font-style: normal; font-variant-caps: normal;" href="https://esco.dtek.com/" rel="nofollow"
                                 target="_blank">Энергосервисные услуги</a>
                                <!--/noindex-->
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </section>

<section class="mobileBanner">
                                                                                                                                    <p>ДТЭК — Лидер ТОПЛИВНО - ЭНЕРГЕТИЧЕСКОГО РЫНКА УКРАИНЫ</p>
                            <ul>
                                            <li><!--noindex--><a href="https://grids.dtek.com/" rel="nofollow" target="_blank">Дистрибуция электроэнергии</a><!--/noindex--></li>
                                                            <li><!--noindex--><a href="https://energo.dtek.com" rel="nofollow" target="_blank">Тепловая энергетика</a><!--/noindex--></li>
                                                            <li><!--noindex--><a href="https://oilandgas.dtek.com" rel="nofollow" target="_blank">Нефть и газ</a><!--/noindex--></li>
                                                            <li><!--noindex--><a href="https://renewables.dtek.com" rel="nofollow" target="_blank">Зеленая энергетика</a><!--/noindex--></li>
                                                            <li><!--noindex--><a href="https://esco.dtek.com" rel="nofollow" target="_blank">энергоэффективность</a><!--/noindex--></li>
                                                                </ul>
    </section>



       <section id="project"  class="project">
            <div class="container">
                <div class="project-details">
                    <div class="row headlines-block">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="head_title">
                                <h2>WHY CHOOSE US?</h2>
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,</p>

                            </div>
                        </div>
                    </div>
                    <div class="project-content">
                        <div class="gallery-content">
                            <div class="isotope">
                                <div class="row">
                                    <div class=" col-md-4 col-sm-12">
                                        <div class="item big-height">
                                            <img src="<?= $this->Url->build('files/Blocks/image/' . $blocks[0]->image); ?>" alt="portfolio image"/>
                                            <div class="isotope-overlay">
                                                <a href="project.html">
                                                    <span class="lnr lnr-link"></span>
                                                    
                                                </a>
                                                <h3>
                                                    <a href="project.html">
                                                        <?= $blocks[0]->headline ?>
                                                    </a>
                                                </h3>
                                                <p><?= $blocks[0]->text ?></p>
                                            </div><!-- /.isotope-overlay -->
                                        </div><!-- /.item -->
                                    </div><!-- /.col -->
                                    <div class="col-md-8 col-sm-12">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-xs-12">
                                                <div class="item">
                                                    <img src="<?= $this->Url->build('files/Blocks/image/' . $blocks[1]->image); ?>" alt="portfolio image"/>
                                                    <div class="isotope-overlay">
                                                        <a href="project.html">
                                                            <span class="lnr lnr-link"></span>
                                                            
                                                        </a>
                                                        <h3>
                                                            <a href="project.html">
                                                                <?= $blocks[1]->headline ?>
                                                            </a>
                                                        </h3>
                                                        <p><?= $blocks[1]->text ?></p>
                                                    </div><!-- /.isotope-overlay -->
                                                </div><!-- /.item -->
                                            </div><!-- /.col -->
                                            <div class="col-lg-6 col-md-6 col-xs-12">
                                                <div class="item">
                                                    <img src="<?= $this->Url->build('files/Blocks/image/' . $blocks[2]->image); ?>" alt="portfolio image"/>
                                                    <div class="isotope-overlay">
                                                        <a href="project.html">
                                                            <span class="lnr lnr-link"></span>
                                                            
                                                        </a>
                                                        <h3>
                                                            <a href="project.html">
                                                                <?= $blocks[2]->headline ?>
                                                            </a>
                                                        </h3>
                                                        <p><?= $blocks[2]->text ?></p>
                                                    </div><!-- /.isotope-overlay -->
                                                </div><!-- /.item -->
                                            </div><!-- /.col -->
                                        </div><!-- /.row-->
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-xs-12">
                                                <div class="item">
                                                    <img src="<?= $this->Url->build('files/Blocks/image/' . $blocks[3]->image); ?>" alt="portfolio image"/>
                                                    <div class="isotope-overlay">
                                                        <a href="project.html">
                                                            <span class="lnr lnr-link"></span>
                                                            
                                                        </a>
                                                        <h3>
                                                            <a href="project.html">
                                                                <?= $blocks[3]->headline ?>
                                                            </a>
                                                        </h3>
                                                        <p><?= $blocks[3]->text ?>
                                                        </p>
                                                    </div><!-- /.isotope-overlay -->
                                                </div><!-- /.item -->
                                            </div><!-- /.col -->
                                            <div class="col-lg-6 col-md-6 col-xs-12">
                                                <div class="item">
                                                    <img src="<?= $this->Url->build('files/Blocks/image/' . $blocks[4]->image); ?>" alt="portfolio image"/>
                                                    <div class="isotope-overlay">
                                                        <a href="project.html">
                                                            <span class="lnr lnr-link"></span>
                                                            
                                                        </a>
                                                        <h3>
                                                            <a href="project.html">
                                                                <?= $blocks[4]->headline ?>
                                                            </a>
                                                        </h3>
                                                        <p><?= $blocks[4]->text ?>
                                                        </p>
                                                    </div><!-- /.isotope-overlay -->
                                                </div><!--/.item -->
                                            </div><!-- /.col -->
                                        </div><!-- /.row-->

                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                                
                            </div><!--/.isotope-->
                        </div><!--/.gallery-content-->
                    </div><!--/.project-content-->

                </div><!--/.project-details-->


               
            </div><!--/.container-->

        </section><!--/.project-->



<section id="eight-cubes">
    <div class="container">
    <div class="cd-hero-slider">
                <div class="was-li selected">                    
                    <div class="cd-full-width">
                        <div class="container-fluid js-tm-page-content" data-page-no="1" data-page-type="gallery">
                            <div class="tm-img-gallery-container">
                                <div class="tm-img-gallery gallery-one">                                                 
                                  <?php foreach ($blocks as $k => $v): ?>                                  
                                    <div class="grid-item">
                                        <figure class="effect-bubba">
                                            <img src="<?= $this->Url->build('files/Blocks/image/' . $v->image); ?>" alt="Image" class="img-fluid tm-img">
                                            <figcaption>
                                                <h2 class="tm-figure-title"><?= $v->headline ?></h2>
                                                <p class="tm-figure-description"><?= $v->text ?></p>
                                                <a href="img/tm-img-01.jpg">View more</a>
                                            </figcaption>           
                                        </figure>
                                    </div>
                                  <?php endforeach ?>  
                                                                                                         
                                </div>                                 
                            </div>
                        </div>                                                    
                    </div>                    
                </div>             

            </div>

</div>
</section>


        <!-- About -->
        <section id="about">
            <div class="container about_bg">
                <div class="row">
                    <div class="col-lg-7 col-md-6">
                        <div class="about_content">
                            <h2>Welcome to Our Company</h2>
                            <h3>Aliquam lacus purus, auctor malesuada</h3>
                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,</p>
                            <p>sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? </p>
                            <a  class="btn know_btn">know more</a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-lg-offset-1">
                        <div class="about_banner">
                            <img src="img/man.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- About end -->

        <!-- Why us -->
        <section id="why_us">
            <div class="container text-center">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="head_title">
                            <h2>WHY CHOOSE US?</h2>
                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,</p>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="why_us_item">
                            <span class="fa fa-leaf"></span>
                            <h4>We deliver quality</h4>
                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni </p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="why_us_item">
                            <span class="fa fa-futbol-o"></span>
                            <h4>Always on time</h4>
                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="why_us_item">
                            <span class="fa fa-group"></span>
                            <h4>We are pasionate</h4>
                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="why_us_item">
                            <span class="fa fa-line-chart"></span>
                            <h4>Professional Services</h4>
                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni</p>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- Why us end -->

        <!-- Services -->
        <section id="services">
            <div class="container">
                <h2>OUR SERVICES</h2>
                <div class="row">
                    <div class="col-md-4">
                        <div class="service_item">
                            <img src="img/service_img1.jpg" alt="Our Services" />
                            <h3>CONSTRUCTION MANAGEMENT</h3>
                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,</p>
                            <a href="#services" class="btn know_btn">know more</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="service_item">
                            <img src="img/service_img2.jpg" alt="Our Services" />
                            <h3>RENOVATION</h3>
                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,</p>
                            <a href="#services" class="btn know_btn">know more</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="service_item">
                            <img src="img/service_img3.jpg" alt="Our Services" />
                            <h3>ARCHITECTURE</h3>
                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,</p>
                            <a href="#services" class="btn know_btn">know more</a>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- Services end -->

        <!-- Portfolio -->
        <section id="portfolio">
            <div class="container portfolio_area text-center">
                <h2>Made with love</h2>
                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,</p>

                <div id="filters">
                    <button class="button is-checked" data-filter="*">Show All</button>
                    <button class="button" data-filter=".buildings">Buildings</button>
                    <button class="button" data-filter=".interior">Interior Design</button>
                    <button class="button" data-filter=".isolation">Isolation</button>
                    <button class="button" data-filter=".plumbing">Plumbing</button>
                </div>
                <!-- Portfolio grid -->     
                <div class="grid">
                    <div class="grid-sizer"></div>
                    <div class="grid-item grid-item--width2 grid-item--height2 buildings plumbing interior">
                        <img alt="" src="img/highligh_img.jpg" >
                        <div class="portfolio_hover_area">
                            <a class="fancybox" href="img/highligh_img.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                            <a href="#"><span class="fa fa-link"></span></a>
                        </div>  
                    </div>

                    <div class="grid-item buildings interior isolation">
                        <img alt="" src="img/portfolio1.jpg" >
                        <div class="portfolio_hover_area">
                            <a class="fancybox" href="img/portfolio1.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                            <a href="#"><span class="fa fa-link"></span></a>
                        </div>   
                    </div>

                    <div class="grid-item interior plumbing isolation">
                        <img alt="" src="img/portfolio2.jpg" >
                        <div class="portfolio_hover_area">
                            <a class="fancybox" href="img/portfolio2.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                            <a href="#"><span class="fa fa-link"></span></a>
                        </div>  
                    </div>

                    <div class="grid-item isolation buildings">
                        <img alt="" src="img/portfolio3.jpg" >
                        <div class="portfolio_hover_area">
                            <a class="fancybox" href="img/portfolio3.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                            <a href="#"><span class="fa fa-link"></span></a>
                        </div>  
                    </div>

                    <div class="grid-item plumbing isolation">
                        <img alt="" src="img/portfolio4.jpg" >
                        <div class="portfolio_hover_area">
                            <a class="fancybox" href="img/portfolio4.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><span class="fa fa-search"></span></a>
                            <a href="#"><span class="fa fa-link"></span></a>
                        </div>  
                    </div>
                </div><!-- Portfolio grid end -->
            </div>
        </section><!-- Portfolio end -->

        <!-- Testimonial -->
        <section id="testimonial">
            <div class="container text-center testimonial_area">
                <h2>Customer Reviews</h2>
                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,</p>

                <div class="row">
                    <div class="col-md-4">
                        <div class="testimonial_item">
                            <div class="testimonial_content text-left">
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,</p>
                            </div>
                            <img src="img/testimonial_img1.png" alt="Testimonial" />
                            <p class="worker_name">john smith</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="testimonial_item">
                            <div class="testimonial_content">
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,</p>
                            </div>
                            <img src="img/testimonial_img2.png" alt="Testimonial" />
                            <p class="worker_name">john smith</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="testimonial_item">
                            <div class="testimonial_content">
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,</p>
                            </div>
                            <img src="img/testimonial_img1.png" alt="Testimonial" />
                            <p class="worker_name">john smith</p>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- Testimonial end -->

        <!-- Contact form -->
        <section id="contact_form">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2>Do you have any questions?</h2>
                        <h2 class="second_heading">Feel free to contact us!</h2>
                    </div>
                    <form role="form" class="form-inline text-right col-md-6" >
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" rows="5" id="msg" placeholder="Message"></textarea>
                        </div>
                        <button type="submit" class="btn submit_btn">Submit</button>
                    </form>             
                </div>
            </div>
        </section><!-- Contact form end -->

        <!-- Footer -->
        <footer>
            <!-- Footer top -->
            <div class="container footer_top">
                <div class="row">
                    <div class="col-lg-4 col-sm-7">
                        <div class="footer_item">
                            <h4>About Company</h4>
                            <img class="logo" src="img/logo.png" alt="Construction" />
                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem</p>

                            <ul class="list-inline footer_social_icon">
                                <li><a href=""><span class="fa fa-facebook"></span></a></li>
                                <li><a href=""><span class="fa fa-twitter"></span></a></li>
                                <li><a href=""><span class="fa fa-youtube"></span></a></li>
                                <li><a href=""><span class="fa fa-google-plus"></span></a></li>
                                <li><a href=""><span class="fa fa-linkedin"></span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-5">
                        <div class="footer_item">
                            <h4>Explore link</h4>
                            <ul class="list-unstyled footer_menu">
                                <li><a href=""><span class="fa fa-play"></span> Our services</a>
                                <li><a href=""><span class="fa fa-play"></span> Meet our team</a>
                                <li><a href=""><span class="fa fa-play"></span> Forum</a>
                                <li><a href=""><span class="fa fa-play"></span> Help center</a>
                                <li><a href=""><span class="fa fa-play"></span> Contact Cekas</a>
                                <li><a href=""><span class="fa fa-play"></span> Privacy Policy</a>
                                <li><a href=""><span class="fa fa-play"></span> Cekas terms</a>
                                <li><a href=""><span class="fa fa-play"></span> Site map</a>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-7">
                        <div class="footer_item">
                            <h4>Latest post</h4>
                            <ul class="list-unstyled post">
                                <li><a href=""><span class="date">20 <small>AUG</small></span>  Luptatum omittantur duo ne mpetus indoctum</a></li>
                                <li><a href=""><span class="date">20 <small>AUG</small></span>  Luptatum omittantur duo ne mpetus indoctum</a></li>
                                <li><a href=""><span class="date">20 <small>AUG</small></span>  Luptatum omittantur duo ne mpetus indoctum</a></li>
                                <li><a href=""><span class="date">20 <small>AUG</small></span>  Luptatum omittantur duo ne mpetus indoctum</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-5">
                        <div class="footer_item">
                            <h4>Contact us</h4>
                            <ul class="list-unstyled footer_contact">
                                <li><a href=""><span class="fa fa-map-marker"></span> 124 New Line, London UK</a></li>
                                <li><a href=""><span class="fa fa-envelope"></span> hello@psdfreebies.com</a></li>
                                <li><a href=""><span class="fa fa-mobile"></span><p>+44 00 00 1234 <br />+44 00 00 1234</p></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- Footer top end -->

            <!-- Footer bottom -->
            <div class="footer_bottom text-center">
                <p class="wow fadeInRight">
                    Made with 
                    <i class="fa fa-heart"></i>
                    by 
                    <a target="_blank" href="http://bootstrapthemes.co">Bootstrap Themes</a> 
                    2016. All Rights Reserved
                </p>
            </div><!-- Footer bottom end -->
        </footer><!-- Footer end -->

     

        <?= $this->Html->script('jquery-1.12.1.min.js') ?>
        <?= $this->Html->script('bootstrap.min.js') ?>
        <?= $this->Html->script('bootsnav.js') ?>
        <?= $this->Html->script('isotope.js') ?>
        <?= $this->Html->script('isotope-active.js') ?>


        
        <?= $this->Html->script('jquery.fancybox.js') ?>

        <?= $this->Html->script('jquery.scrollUp.min.js') ?>
        <?= $this->Html->script('main.js') ?>


<script type="text/javascript">
    $(document).on('ready', function(){
       
       $('.item').hover(function(e) {
            e.stopPropagation();
            $('#mainBanners').removeClass().addClass($(this).data('section'));
            $('h1.default').fadeOut('fast');
        },function(e) {
            e.stopPropagation();
            $('#mainBanners').removeClass();
            setTimeout(function(){ 
                if ($('.item:hover').length == 0 ) {
                    $('h1.default').fadeIn('fast');
                }                    
            }, 1000);                       
        });
    });
</script>


    </body> 
</html> 