<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BlockItem[]|\Cake\Collection\CollectionInterface $blockItems
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Block Item'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Blocks'), ['controller' => 'Blocks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Block'), ['controller' => 'Blocks', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="blockItems index large-9 medium-8 columns content">
    <h3><?= __('Block Items') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('block_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('text') ?></th>
                <th scope="col"><?= $this->Paginator->sort('link') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($blockItems as $blockItem): ?>
            <tr>
                <td><?= $this->Number->format($blockItem->id) ?></td>
                <td><?= $blockItem->has('block') ? $this->Html->link($blockItem->block->id, ['controller' => 'Blocks', 'action' => 'view', $blockItem->block->id]) : '' ?></td>
                <td><?= h($blockItem->text) ?></td>
                <td><?= h($blockItem->link) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $blockItem->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $blockItem->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $blockItem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $blockItem->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
