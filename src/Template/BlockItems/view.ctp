<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BlockItem $blockItem
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Block Item'), ['action' => 'edit', $blockItem->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Block Item'), ['action' => 'delete', $blockItem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $blockItem->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Block Items'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Block Item'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Blocks'), ['controller' => 'Blocks', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Block'), ['controller' => 'Blocks', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="blockItems view large-9 medium-8 columns content">
    <h3><?= h($blockItem->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Block') ?></th>
            <td><?= $blockItem->has('block') ? $this->Html->link($blockItem->block->id, ['controller' => 'Blocks', 'action' => 'view', $blockItem->block->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Text') ?></th>
            <td><?= h($blockItem->text) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Link') ?></th>
            <td><?= h($blockItem->link) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($blockItem->id) ?></td>
        </tr>
    </table>
</div>
