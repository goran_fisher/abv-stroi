<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BlockItem $blockItem
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Block Items'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Blocks'), ['controller' => 'Blocks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Block'), ['controller' => 'Blocks', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="blockItems form large-9 medium-8 columns content">
    <?= $this->Form->create($blockItem) ?>
    <fieldset>
        <legend><?= __('Add Block Item') ?></legend>
        <?php
            echo $this->Form->control('block_id', ['options' => $blocks]);
            echo $this->Form->control('text');
            echo $this->Form->control('link');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
