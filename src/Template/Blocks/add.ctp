<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Block $block
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Blocks'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="blocks form large-9 medium-8 columns content">
    <?= $this->Form->create($block, ['type' => 'file']) ?>
   
       
        <?php
            echo $this->Form->control('headline',['class' => 'form-control']);
            echo $this->Form->control('text',['class' => 'form-control']);
            echo $this->Form->control('image', ['type' => 'file']);
        ?>
   
<br><br>
    <?php $hasLinks = true ?>
    <?php if ($hasLinks): ?>
       
        


    <div class="box box-primary" style="max-width: 430px;display: none">
        <div class="box-header with-border">
          <h3 class="box-title">Box Link</h3>
          <i style="float:right;" class="glyphicon glyphicon-trash"></i>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        
          <div class="box-body">
             <div class="form-group" style="display:inline">
              <label for="exampleInputEmail1">Text</label>
              <input disabled type="text" name="links[1][text]" class="form-control" style="width: 400px;" placeholder="Enter text" required="required" maxlength="255">
            </div>


            <div class="form-group" style="display:inline">
              <label for="exampleInputEmail1">Link</label>
              <input disabled type="text" name="links[1][link]" class="form-control" style="width: 400px" placeholder="Enter link">
            </div>

          </div>
          <!-- /.box-body -->            
        
      </div>

    <div class="box box-primary" style="max-width: 430px;display: none">
        <div class="box-header with-border">
          <h3 class="box-title">Box Link</h3>
          <i style="float:right;" class="glyphicon glyphicon-trash"></i>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        
          <div class="box-body">
             <div class="form-group" style="display:inline">
              <label for="exampleInputEmail1">Text</label>
              <input disabled type="text" name="links[2][text]" class="form-control" style="width: 400px;" placeholder="Enter text" required="required" maxlength="255">
            </div>


            <div class="form-group" style="display:inline">
              <label for="exampleInputEmail1">Link</label>
              <input disabled type="text" name="links[2][link]" class="form-control" style="width: 400px" placeholder="Enter link">
            </div>

          </div>
          <!-- /.box-body -->            
        
      </div>

    <div class="box box-primary" style="max-width: 430px;display: none">
        <div class="box-header with-border">
          <h3 class="box-title">Box Link</h3>
          <i style="float:right;" class="glyphicon glyphicon-trash"></i>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        
          <div class="box-body">
             <div class="form-group" style="display:inline">
              <label for="exampleInputEmail1">Text</label>
              <input disabled type="text" name="links[3][text]" class="form-control" style="width: 400px;" placeholder="Enter text" required="required" maxlength="255">
            </div>


            <div class="form-group" style="display:inline">
              <label for="exampleInputEmail1">Link</label>
              <input disabled type="text" name="links[3][link]" class="form-control" style="width: 400px" placeholder="Enter link">
            </div>

          </div>
          <!-- /.box-body -->            
        
      </div>

    <div class="box box-primary" style="max-width: 430px;display: none">
        <div class="box-header with-border">
          <h3 class="box-title">Box Link</h3>
          <i style="float:right;" class="glyphicon glyphicon-trash"></i>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        
          <div class="box-body">
             <div class="form-group" style="display:inline">
              <label for="exampleInputEmail1">Text</label>
              <input disabled type="text" name="links[4][text]" class="form-control" style="width: 400px;" placeholder="Enter text" required="required" maxlength="255">
            </div>


            <div class="form-group" style="display:inline">
              <label for="exampleInputEmail1">Link</label>
              <input disabled type="text" name="links[4][link]" class="form-control" style="width: 400px" placeholder="Enter link">
            </div>

          </div>
          <!-- /.box-body -->            
        
      </div>
<br>
        <i id="addLink" class="glyphicon glyphicon-plus"></i>
    <br>   


    <?php endif ?>





    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<script type="text/javascript">
    $('#addLink').on('click', function(e){
        $('.box-primary:hidden').first().show().find('input').removeAttr('disabled');
    });

    $('.glyphicon-trash').on('click', function(e){
        $(this).closest('.box-primary').hide().find('input').attr('disabled', 'disabled');
    });
</script>