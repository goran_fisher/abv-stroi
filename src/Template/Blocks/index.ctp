<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Block[]|\Cake\Collection\CollectionInterface $blocks
 */
?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Block'), ['action' => 'add']) ?></li>
    </ul>
</nav>

<div style="margin: 0 auto;width: 800px">
    

<div class="row">
    <div class="col-md-3">
        <div style="width: 215px;height: 200px;background-color: red;border: 5px solid white;">
           <p style="text-align: center;
vertical-align: middle;
line-height: 190px;">click to edit</p> 
        </div>
        <div style="width: 215px;height: 100px;background-color: red;border: 5px solid white;">
            <p style="text-align: center;
vertical-align: middle;
line-height: 90px;">click to edit</p>
        </div>
    </div>

     <div class="col-md-6">

        <div class="row">
            <div class="col-md-6">
                <div style="width: 215px;height: 150px;background-color: red;border: 5px solid white;">
                <p style="text-align: center;
vertical-align: middle;
line-height: 140px;">click to edit</p>
                </div>              

            </div>

            <div class="col-md-6">
                 <div style="width: 215px;height: 150px;background-color: red;border: 5px solid white;">
                    <p style="text-align: center;
vertical-align: middle;
line-height: 140px;">click to edit</p>
                </div>
            </div>

        </div>

           <div class="row">
            <div class="col-md-6">
                <div style="width: 215px;height: 150px;background-color: red;border: 5px solid white;">
                <p style="text-align: center;
vertical-align: middle;
line-height: 140px;">click to edit</p>
                </div>              

            </div>

            <div class="col-md-6">
                 <div style="width: 215px;height: 150px;background-color: red;border: 5px solid white;">
                   <p style="text-align: center;
vertical-align: middle;
line-height: 140px;">click to edit</p> 
                </div>
            </div>

        </div>

       
    </div>


</div>


</div>




<div class="blocks index large-9 medium-8 columns content">
    <h3><?= __('Blocks') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('headline') ?></th>
                <th scope="col"><?= $this->Paginator->sort('text') ?></th>
                <th scope="col"><?= $this->Paginator->sort('image') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($blocks as $block): ?>
            <tr>
                <td><?= $this->Number->format($block->id) ?></td>
                <td><?= h($block->headline) ?></td>
                <td><?= h($block->text) ?></td>
                <td><?= h($block->image) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $block->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $block->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $block->id], ['confirm' => __('Are you sure you want to delete # {0}?', $block->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
