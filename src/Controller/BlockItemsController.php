<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * BlockItems Controller
 *
 * @property \App\Model\Table\BlockItemsTable $BlockItems
 *
 * @method \App\Model\Entity\BlockItem[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BlockItemsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Blocks']
        ];
        $blockItems = $this->paginate($this->BlockItems);

        $this->set(compact('blockItems'));
    }

    /**
     * View method
     *
     * @param string|null $id Block Item id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $blockItem = $this->BlockItems->get($id, [
            'contain' => ['Blocks']
        ]);

        $this->set('blockItem', $blockItem);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $blockItem = $this->BlockItems->newEntity();
        if ($this->request->is('post')) {
            $blockItem = $this->BlockItems->patchEntity($blockItem, $this->request->getData());
            if ($this->BlockItems->save($blockItem)) {
                $this->Flash->success(__('The block item has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The block item could not be saved. Please, try again.'));
        }
        $blocks = $this->BlockItems->Blocks->find('list', ['limit' => 200]);
        $this->set(compact('blockItem', 'blocks'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Block Item id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $blockItem = $this->BlockItems->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $blockItem = $this->BlockItems->patchEntity($blockItem, $this->request->getData());
            if ($this->BlockItems->save($blockItem)) {
                $this->Flash->success(__('The block item has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The block item could not be saved. Please, try again.'));
        }
        $blocks = $this->BlockItems->Blocks->find('list', ['limit' => 200]);
        $this->set(compact('blockItem', 'blocks'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Block Item id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $blockItem = $this->BlockItems->get($id);
        if ($this->BlockItems->delete($blockItem)) {
            $this->Flash->success(__('The block item has been deleted.'));
        } else {
            $this->Flash->error(__('The block item could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
